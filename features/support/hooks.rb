Before do
  @appium_driver = Appium::Driver.new(opts, true)
  Appium.promote_appium_methods Object
  @settings = YAML.load_file(File.expand_path('../../cucumber.yml', File.dirname(__FILE__)))
  $driver.start_driver
end

After do |scenario|
  scenario_name = scenario.name.gsub(/\s+/, '_').tr('/', '_')
  scenario_name = scenario_name.delete(',', '')
  scenario_name = scenario_name.delete('(', '')
  scenario_name = scenario_name.delete(')', '')
  scenario_name = scenario_name.delete('#', '')

  if scenario.passed?
    unless File.directory?('results/screenshots/test_passed')
      FileUtils.mkdir_p('results/screenshots/test_passed')
    end
    screenshot_name = "#{scenario_name.downcase!}.png"
    screenshot_file = File.join('results/screenshots/test_passed',
                                screenshot_name)
    screenshot(screenshot_file)
    embed(screenshot_file, 'image/png')
  elsif scenario.failed?
    unless File.directory?('results/screenshots/test_failed')
      FileUtils.mkdir_p('results/screenshots/test_failed')
    end
    screenshot_name = "#{scenario_name.downcase!}.png"
    screenshot_file = File.join('results/screenshots/test_failed',
                                screenshot_name)
    $driver.screenshot(screenshot_file)
    embed(screenshot_file, 'image/png')
  end
end

AfterConfiguration do
  FileUtils.rm_r('results/screenshots/test_failed/') if File.directory?(
    'results/screenshots/test_failed/'
  )
  FileUtils.rm_r('results/screenshots/test_passed/') if File.directory?(
    'results/screenshots/test_passed/'
  )
end
at_exit do
  t = Time.now
  @data =  t.strftime("%d/%m/%Y")
  @time =  t.strftime("%H:%M:%S")
  ReportBuilder.configure do |config|
  config.json_path = 'results/report.json'
  config.report_path = 'results/cucumber_web_report'
  config.report_types = [:html]
  config.report_tabs = %w[Overview Features Scenarios Errors]
  config.report_title = 'Report Automation bs2 - Cartões'
  config.compress_images = false
  config.color = "dark blue"
  config.additional_info = { 'Projeto' => 'bs2 Cartões', 'Platforma' => 'Android',  'Data' => @data, 'Hora' => @time  }
  end
  ReportBuilder.build_report
end